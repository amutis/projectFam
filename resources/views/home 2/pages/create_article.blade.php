@extends('blog.main')

@section('styles')
    <link href="/blog/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/blog/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
    <link href="/blog/font-awesome/css/font-awesome.css" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12" style="background-color: whitesmoke">
            <div class="ibox float-e-margins">
                <div class="ibox-content no-padding">
                {!! Form::open(['route' => 'post_article']) !!}
                    <input type="text" name="article" class="summernote">
                    <button class="btn btn-primary" type="submit">Publish</button>
                 {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>


@stop

@section('scripts')
    <!-- SUMMERNOTE -->
    <script src="/blog/js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(document).ready(function(){

            $('.summernote').summernote();

        });
        var edit = function() {
            $('.click2edit').summernote({focus: true});
        };
        var save = function() {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };
    </script>


@stop