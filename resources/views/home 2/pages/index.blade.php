@extends('blog.main')

@section('content')
<div class="col-md-8 about-left">
    <div class="about-one">
        <h3>Coffee of the month</h3>
    </div>
    <div class="about-two">
        <a href="single.html"><img src="/blog/images/c-1.jpg" alt="" /></a>
        <p>Posted by <a href="#">Johnson</a> on 10 feb, 2015 <a href="#">comments(2)</a></p>
        <p>Phasellus fringilla enim nibh, ac pharetra nulla vestibulum ac. Donec tempor fermentum felis, non placerat sem ultrices ut. Nam molestie nunc nec felis hendrerit, in pulvinar arcu mollis. Quisque eget purus nec velit venenatis tincidunt vitae ac massa. Proin vel ornare tellus. Duis consectetur gravida tellus ut varius. Aenean tellus massa, laoreet ut euismod et, pretium id ex. Mauris hendrerit suscipit hendrerit.</p>
        <p>Quisque ultrices ligula a nisl porttitor, vitae porta tortor eleifend. Nulla nec imperdiet ipsum, ut cursus mauris. Proin ut sodales sem, quis vestibulum libero. Proin tempor venenatis congue. Phasellus mollis massa sit amet pharetra consequat. Aliquam quis lacus at sapien tempor semper. Sed ultrices et metus et elementum. Nunc sed justo at erat consequat mollis et eu lectus.</p>
        <div class="about-btn">
            <a href="single.html">Read More</a>
        </div>
    </div>
    <div class="about-tre">
        <div class="a-1">
            @foreach(\App\Post::inRandomOrder()->limit(6)->get() as $article)
            <div class="col-md-6 abt-left">
                <a href="single.html"><img src="/blog/images/c-3.jpg" alt="" /></a>
                <h6>{{$article->category->name}}</h6>
                <h3><a href="single.html">{{$article->title}}</a></h3>
                <p>{{substr($article->post,0,40)}}...</p>
                <label>{{date('M d, Y',strtotime($article->created_at))}}</label>
            </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="col-md-4 about-right heading">
    @include('blog.partials._side')
</div>
@stop