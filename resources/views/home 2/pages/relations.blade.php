@extends('blog.main')

@section('content')
    @if(Auth::user()->mobile == null)
    <div class="row">
        <div class="col-lg-12" >
            <div class="ibox float-e-margins">
                <div class="ibox-content no-padding">
                {!! Form::open(['route' => 'update_information']) !!}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <label for="">First Name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" disabled value="{{Auth::user()->first_name}}" name="family_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <label for="">Last Name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" disabled value="{{Auth::user()->last_name}}" name="family_name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <label for="">Date of Birth</label>
                            </div>
                            <div class="col-md-8">
                                <input type="date"  value="{{old('date_of_birth')}}" placeholder="yyyy-mm-dd" name="date_of_birth" class="form-control">
                                @if ($errors->has('date_of_birth'))
                                    <span  style="color: red" class="help-block">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <label for="">County Information</label>
                            </div>
                            <div class="col-md-8">
                                <select name="county" id="" class="form-control">
                                    <option value="">Select Your County</option>
                                    <option value=""></option>
                                    @foreach(\App\County::all() as $county)
                                    <option value="{{encrypt($county->id)}}">{{$county->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('county'))
                                    <span style="color: red" class="help-block">
                                        <strong>{{ $errors->first('county') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <label for="">Family Name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="family_name" value="{{old('family_name')}}" class="form-control">
                                @if ($errors->has('family_name'))
                                    <span style="color: red" class="help-block">
                                        <strong>{{ $errors->first('family_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <label for="">Phone Number</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="phone_number" value="{{old('phone_number')}}" required placeholder="2547..." class="form-control">
                                @if ($errors->has('phone_number'))
                                    <span style="color: red" class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                    </div>


                    <button class="btn btn-primary" type="submit">Update Information</button>
                 {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
    @else
        @include('blog.partials.my_people')
        <br>
        <br>
        <br>
        <table class="table">
            <tr>
                <td>Relation Name</td>
                <td>Phone Number</td>
                <td>Name</td>
                <td>Approval</td>
                <td>Action</td>
            </tr>
            @foreach(\App\PhoneRelation::where('user_id',Auth::user()->id)->orwhere('phone_number',Auth::user()->mobile)->get() as $relation)
               @if($relation->user_id == Auth::user()->id)
                    <tr>
                        <td>{{$relation->relationship_type->name}}</td>
                        <td>{{$relation->phone_number}}</td>
                        <td>
                            @if(\App\User::where('mobile',$relation->phone_number)->first())
                                {{$relation->user->first_name}} {{$relation->user->last_name}}
                            @else
                                Your {{$relation->relationship_type->name}} has not joined yet
                            @endif
                        </td>
                        <td>
                            @if($relation->approval == 0)

                            @else

                            @endif
                        </td>
                        <td>
                            @if(\App\User::where('mobile',$relation->phone_number)->first())

                            @else
                                <a href=""> Notify your {{$relation->relationship_type->name}}</a>
                            @endif
                        </td>
                    </tr>
               @else
                    <tr>
                        <td>{{$relation->relationship_type->name}}</td>
                        <td>{{$relation->owner->mobile}}</td>
                        <td>
                                {{$relation->owner->first_name}} {{$relation->owner->last_name}}
                        </td>
                        <td></td>
                        <td>
                        </td>
                    </tr>
               @endif
            @endforeach
        </table>
    @endif

@stop

