
<!DOCTYPE html>
<html>
<head>
   @include('blog.partials._header')
</head>
<body>

    <!--header-top-starts-->
    <div class="header-top">
        <div class="container">
            <div class="head-main">
                <a href="/">Project Fam</a>
            </div>
        </div>
    </div>

<!--header-top-end-->
<!--start-header-->
<div class="header">
    @include('blog.partials._nav')
</div>
<!-- script-for-menu -->
<!-- script-for-menu -->
<script>
    $("span.menu").click(function(){
        $(" ul.navig").slideToggle("slow" , function(){
        });
    });
</script>
<!-- script-for-menu -->
<!--banner-starts-->
    @if(Request::is('relations'))
    @else
<div class="banner">
    <div class="container">
        <div class="banner-top">
            <div class="banner-text">
                <h2>Aliquam erat</h2>
                <h1>Suspendisse potenti</h1>
                <div class="banner-btn">
                    <a href="single.html">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
    @endif
<!--banner-end-->
<!--about-starts-->
<div class="about">
    <div class="container">
        <div class="about-main">
            @yield('content')

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--about-end-->
<!--slide-starts-->
<div class="slide">
    <div class="container">
        <div class="fle-xsel">
            <ul id="flexiselDemo3">
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="/blog/images/s-1.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="/blog/images/s-2.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="/blog/images/s-3.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="/blog/images/s-4.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="/blog/images/s-5.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="/blog/images/s-6.jpg" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
            </ul>

            <script type="text/javascript">
                $(window).load(function() {

                    $("#flexiselDemo3").flexisel({
                        visibleItems: 5,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint:480,
                                visibleItems: 2
                            },
                            landscape: {
                                changePoint:640,
                                visibleItems: 3
                            },
                            tablet: {
                                changePoint:768,
                                visibleItems: 3
                            }
                        }
                    });

                });
            </script>
            <script type="text/javascript" src="/blog/js/jquery.flexisel.js"></script>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--slide-end-->
<!--footer-starts-->
<div class="footer">
    <div class="container">
        <div class="footer-text">
            <p>© 2015 Coffee Break. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
        </div>
    </div>
</div>
<!--footer-end-->

<script src="/blog/js/bootstrap.min.js"></script>
@yield('scripts')


</body>
</html>