<div class="container">
    <div class="head">
        <div class="navigation">
            <span class="menu"></span>
            <ul class="navig">
                <li><a href="/"  class="active">Home</a></li>
                <li><a href="about.html">About</a></li>
                <li><a href="gallery.html">Gallery</a></li>
                <li><a href="typo.html">Family Trees</a></li>
                @if(Auth::check())
                    <li><a href="{{route('post_article')}}">Post Article</a></li>
                    <li><a href="{{route('relations')}}">My Relations</a></li>
                    <li><a href="/logout">Logout</a></li>
                @else
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li>
                @endif

            </ul>
        </div>
        <div class="header-right">
            <div class="search-bar">
                <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                <input type="submit" value="">
            </div>
            <ul>
                <li><a href="#"><span class="fb"> </span></a></li>
                <li><a href="#"><span class="twit"> </span></a></li>
                <li><a href="#"><span class="pin"> </span></a></li>
                <li><a href="#"><span class="rss"> </span></a></li>
                <li><a href="#"><span class="drbl"> </span></a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>