
{!! Form::open(['route' => 'add_relationship']) !!}
<div class="row">
    <div class="col-md-5">
        <div class="col-md-4">
            <label for="">Relation Type</label>
        </div>
        <div class="col-md-8">
            <select name="relation_type" id="" class="form-control">
                <option value=""></option>
                @foreach(\App\RelationshipType::all() as $type)
                    <option value="{{encrypt($type->id)}}">{{$type->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('relation_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('relation_type') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-4">
            <label for="">Phone Number</label>
        </div>
        <div class="col-md-8">
            <input type="text" name="phone_number" value="{{old('phone_number')}}" placeholder="2547..."  class="form-control">
            @if ($errors->has('phone_number'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone_number') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-1">
        <button class="btn btn-primary" type="submit">Add</button>
    </div>
</div>
 {!! Form::close() !!}

