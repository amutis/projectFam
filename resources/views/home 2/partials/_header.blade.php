<title>Project Fam</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="/blog/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="/blog/css/style.css" rel='stylesheet' type='text/css' />

@yield('styles')
<script src="/blog/js/jquery.min.js"></script>
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="/blog/js/move-top.js"></script>
<script type="text/javascript" src="/blog/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!--start-smoth-scrolling-->

