<!DOCTYPE html>
<html>
<head>
    @include('home.partials.header')
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    @include('home.partials.nav')
    <!-- Full Width Column -->
        <div class="content-wrapper">
        @include('home.messages')
        @yield('content')
        </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>IS Project Version</b> 0.9
            </div>
            <strong>Copyright &copy; {{date('Y')}} <a href="http://techfyglobal.com">Muhamed Hish</a>.</strong> All rights
            reserved.
        </div>
        <!-- /.container -->
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/lte/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/lte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/lte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/lte/dist/js/demo.js"></script>
</body>
</html>

