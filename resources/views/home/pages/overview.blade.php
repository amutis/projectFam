@extends('home.main')

@section('contdent')
    <div class="row">
        <div class="content-wrapper">
            <div class="row" style="margin-left: 2%;margin-right: 2%">
                <div class="col-lg-3 col-xs-6" style="margin-top: 2%">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{\App\PhoneRelation::where('phone_number',Auth::user()->mobile)->orwhere('user_id',Auth::user()->id)->where('approval',1)->count()}}</h3>

                            <p>Immediate Family</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <a href="{{route('relations')}}" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
            {{--<div class="col-lg-3 col-xs-6" style="margin-top: 2%">--}}
            {{--<!-- small box -->--}}
            {{--<div class="small-box bg-green">--}}
            {{--<div class="inner">--}}
            {{--<h3>--}}
            {{--@php($x = 0)--}}
            {{--@php($father = [])--}}
            {{--@php($mother = [])--}}
            {{--@if(\App\PhoneRelation::where('relationship_type_id',1)->first() != null)--}}
            {{--@php($father = \App\PhoneRelation::where('relationship_type_id',1)->where('approval',1)->first())--}}
            {{--@foreach(\App\PhoneRelation::wherein('relationship_type_id',[2,3])->where('user_id',$father->id)->where('approval',0)->get() as $father_rel)--}}
            {{--@foreach(\App\PhoneRelation::wherein('relationship_type_id',[7,8])->where('user_id',Auth::user()->id)->where('approval',0)->get() as $child)--}}
            {{--@php($x++)--}}
            {{--@endforeach--}}
            {{--@endforeach--}}
            {{--@elseif(\App\PhoneRelation::where('phone_number',Auth::user()->mobile)->first() != null)--}}
            {{--@endif--}}
            {{--@if(\App\PhoneRelation::where('relationship_type_id',2)->first() != null)--}}
            {{--@php($mother = \App\PhoneRelation::where('relationship_type_id',2)->where('approval',1)->first())--}}
            {{--@foreach(\App\PhoneRelation::wherein('relationship_type_id',[2,3])->where('user_id',$mother->id)->where('approval',0)->get() as $father_rel)--}}
            {{--@foreach(\App\PhoneRelation::wherein('relationship_type_id',[7,8])->where('user_id',Auth::user()->id)->where('approval',0)->get() as $child)--}}
            {{--@php($x++)--}}
            {{--@endforeach--}}
            {{--@endforeach--}}
            {{--@endif--}}


            {{--{{$mother}}--}}

            {{--</h3>--}}

            {{--<p>Cousins</p>--}}
            {{--</div>--}}
            {{--<div class="icon">--}}
            {{--<i class="ion ion-stats-bars"></i>--}}
            {{--</div>--}}
            {{--<a href="#" class="small-box-footer">--}}
            {{--More info <i class="fa fa-arrow-circle-right"></i>--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <!-- ./col -->
                <div class="col-lg-3 col-xs-6" style="margin-top: 2%">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>
                                {{\App\PhoneRelation::wherein('relationship_type_id',[7,8])->where('user_id',Auth::user()->id)->where('approval',0)->count()}}
                            </h3>

                            <p>Children</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6" style="margin-top: 2%">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                            More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div>
    </div>

@stop

@section('content')
    <style>
        /*Now the CSS*/
        * {margin: 0; padding: 0;}

        .tree ul {
            padding-top: 20px; position: relative;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        .tree li {
            float: left; text-align: center;
            list-style-type: none;
            position: relative;
            padding: 20px 5px 0 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        /*We will use ::before and ::after to draw the connectors*/

        .tree li::before, .tree li::after{
            content: '';
            position: absolute; top: 0; right: 50%;
            border-top: 1px solid #ccc;
            width: 50%; height: 20px;
        }
        .tree li::after{
            right: auto; left: 50%;
            border-left: 1px solid #ccc;
        }

        /*We need to remove left-right connectors from elements without
        any siblings*/
        .tree li:only-child::after, .tree li:only-child::before {
            display: none;
        }

        /*Remove space from the top of single children*/
        .tree li:only-child{ padding-top: 0;}

        /*Remove left connector from first child and
        right connector from last child*/
        .tree li:first-child::before, .tree li:last-child::after{
            border: 0 none;
        }
        /*Adding back the vertical connector to the last nodes*/
        .tree li:last-child::before{
            border-right: 1px solid #ccc;
            border-radius: 0 5px 0 0;
            -webkit-border-radius: 0 5px 0 0;
            -moz-border-radius: 0 5px 0 0;
        }
        .tree li:first-child::after{
            border-radius: 5px 0 0 0;
            -webkit-border-radius: 5px 0 0 0;
            -moz-border-radius: 5px 0 0 0;
        }

        /*Time to add downward connectors from parents*/
        .tree ul ul::before{
            content: '';
            position: absolute; top: 0; left: 50%;
            border-left: 1px solid #ccc;
            width: 0; height: 20px;
        }

        .tree li a{
            border: 1px solid #ccc;
            padding: 5px 10px;
            text-decoration: none;
            color: #666;
            font-family: arial, verdana, tahoma;
            font-size: 11px;
            display: inline-block;

            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        /*Time for some hover effects*/
        /*We will apply the hover effect the the lineage of the element also*/
        .tree li a:hover, .tree li a:hover+ul li a {
            background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
        }
        /*Connector styles on hover*/
        .tree li a:hover+ul li::after,
        .tree li a:hover+ul li::before,
        .tree li a:hover+ul::before,
        .tree li a:hover+ul ul::before{
            border-color:  #94a0b4;
        }

        /*Thats all. I hope you enjoyed it.
        Thanks :)*/
    </style>
    <div class="row">
        <div class="content-wrapper">

            <div class="tree" style="position: absolute; margin-left: 30%">
                <ul>
                    <li>
                        <a href="#">You
                            @if(\App\Spouse::where('user_id',Auth::user()->id)->first() != null)
                                & {{\App\Spouse::where('user_id',Auth::user()->id)->first()->spouse_detail->first_name}} {{\App\Spouse::where('user_id',Auth::user()->id)->first()->spouse_detail->last_name}}
                            @endif
                        </a>
                        <ul>
                            @foreach(\App\Child::where('user_id',Auth::user()->id)->get() as $child)
                            <li>
                                <a href="#">{{$child->child_detail->first_name}} {{$child->child_detail->last_name}}
                                    @if(\App\Spouse::where('user_id',$child->child_detail->id)->first() != null)
                                        & {{\App\Spouse::where('user_id',$child->child_detail->id)->first()->spouse_detail->first_name}} {{\App\Spouse::where('user_id',$child->child_detail->id)->first()->spouse_detail->last_name}}
                                    @endif
                                </a>

                                <ul>
                                    @foreach(\App\Child::where('user_id',$child->child_detail->id)->get() as $child)
                                    <li>
                                        <a href="#">{{$child->child_detail->first_name}} {{$child->child_detail->last_name}}
                                            @if(\App\Spouse::where('user_id',$child->child_detail->id)->first() != null)
                                                & {{\App\Spouse::where('user_id',$child->child_detail->id)->first()->spouse_detail->first_name}} {{\App\Spouse::where('user_id',$child->child_detail->id)->first()->spouse_detail->last_name}}
                                            @endif
                                        </a>
                                        <ul>
                                            @foreach(\App\Child::where('user_id',$child->child_detail->id)->get() as $child)
                                                <li>
                                                    <a href="#">{{$child->child_detail->first_name}} {{$child->child_detail->last_name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @foreach(\App\Sibling::where('user_id',Auth::user()->id)->get() as $sibling)
                        <li>
                            <a href="#">{{$sibling->sibling_detail->first_name}}
                                @if(\App\Spouse::where('user_id',$sibling->sibling_detail->id)->first() != null)
                                    & {{\App\Spouse::where('user_id',$sibling->sibling_detail->id)->first()->spouse_detail->first_name}} {{\App\Spouse::where('user_id',$sibling->sibling_detail->id)->first()->spouse_detail->last_name}}
                                @endif
                            </a>

                            <ul>
                                @foreach(\App\Child::where('user_id',$sibling->sibling_id)->get() as $child)
                                    <li>
                                        <a href="#">{{$child->child_detail->first_name}} {{$child->child_detail->last_name}}</a>
                                        <ul>
                                            @foreach(\App\Child::where('user_id',$child->child_detail->id)->get() as $child)
                                                <li>
                                                    <a href="#">{{$child->child_detail->first_name}} {{$child->child_detail->last_name}}</a>
                                                    <ul>
                                                        @foreach(\App\Child::where('user_id',$child->child_detail->id)->get() as $child)
                                                            <li>
                                                                <a href="#">{{$child->child_detail->first_name}} {{$child->child_detail->last_name}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@stop