@extends('home.main')

@section('content')
    @if(Auth::user()->mobile == null)
        <div class="row">
           <!-- right column -->
           <div class="col-md-10 col-md-offset-1" style="margin-top: 2%">
               <!-- Horizontal Form -->
               <div class="box box-info">
                   <div class="box-header with-border">
                       <h3 class="box-title"><strong>{{strtoupper(Auth::user()->first_name)}}</strong>, complete your information to add relations</h3>
                   </div>
                   <!-- /.box-header -->
                   <!-- form start -->
                   {!! Form::open(['route' => 'update_information']) !!}
                       <div class="box-body">
                           <div class="row">
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <label for="inputEmail3" class="col-sm-3 control-label">First Name</label>

                                       <div class="col-sm-9">
                                           <input type="text" disabled value="{{Auth::user()->first_name}}" class="form-control" id="inputEmail3" placeholder="Email">
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>

                                       <div class="col-sm-10">
                                           <input type="text" disabled value="{{Auth::user()->last_name}}" class="form-control" id="inputEmail3" placeholder="Email">
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <br>
                           <div class="row">
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <label for="inputEmail3" class="col-sm-3 control-label">Date of Birth</label>

                                       <div class="col-sm-9">
                                           <input type="date" name="date_of_birth"  value="{{old('date_of_birth')}}" class="form-control" id="inputEmail3" placeholder="yyyy-mm-dd">
                                           @if ($errors->has('date_of_birth'))
                                               <span  style="color: red" class="help-block">
                                                    <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                </span>
                                           @endif
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <label for="inputEmail3" class="col-sm-2 control-label">County</label>

                                       <div class="col-sm-10">
                                           <select name="county" id="" class="form-control">
                                               <option value="">Select Your County</option>
                                               <option value=""></option>
                                               @foreach(\App\County::all() as $county)
                                                   <option value="{{encrypt($county->id)}}">{{$county->name}}</option>
                                               @endforeach
                                           </select>
                                           @if ($errors->has('county'))
                                               <span style="color: red" class="help-block">
                                            <strong>{{ $errors->first('county') }}</strong>
                                        </span>
                                           @endif
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   <br>
                   <div class="row">
                       <div class="col-md-6">
                           <div class="form-group">
                               <label for="inputEmail3" class="col-sm-3 control-label">Family Name</label>

                               <div class="col-sm-9">
                                   <input type="text" name="family_name"  value="{{old('family_name')}}" class="form-control" id="inputEmail3" placeholder="Family Name">
                                   @if ($errors->has('family_name'))
                                       <span  style="color: red" class="help-block">
                                                    <strong>{{ $errors->first('family_name') }}</strong>
                                                </span>
                                   @endif
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="form-group">
                               <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>

                               <div class="col-sm-10">
                                   <input type="text" name="phone_number"  value="{{old('phone_number')}}" class="form-control" id="inputEmail3" placeholder="2547...">
                                   @if ($errors->has('phone_number'))
                                       <span  style="color: red" class="help-block">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                   @endif
                               </div>
                           </div>
                       </div>
                   </div>
                   <br>
                       <!-- /.box-body -->
                       <div class="box-footer">
                           <button type="reset" class="btn btn-default">Cancel</button>
                           <button type="submit" class="btn btn-info pull-right">Update Information</button>
                       </div>
                       <!-- /.box-footer -->
                   {!! Form::close() !!}
               </div>
           </div>
        </div>
    @else
        <div class="row">
            <!-- right column -->
            <div class="col-md-10 col-md-offset-1" style="margin-top: 2%">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>{{strtoupper(Auth::user()->first_name)}}</strong>, Add relations below</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['route' => 'add_relationship']) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Relation Type</label>

                                    <div class="col-sm-9">
                                        <select name="relation_type" id="" class="form-control">
                                            <option value="">Select Relationship</option>
                                            <option value=""></option>
                                            @foreach(\App\RelationshipType::all() as $type)
                                                <option value="{{encrypt($type->id)}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('relation_type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('relation_type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="phone_number" value="{{old('phone_number')}}" placeholder="2547..."  class="form-control">
                                        @if ($errors->has('phone_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Update Information</button>
                    </div>
                    <!-- /.box-footer -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="row">
            <!-- right column -->
            <div class="col-md-10 col-md-offset-1" style="margin-top: 2%">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>{{strtoupper(Auth::user()->first_name)}}</strong>, below are your relations</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Relation Name</th>
                                <th>Phone Number</th>
                                <th>Name</th>
                                <th>Approval</th>
                                <th>Action</th>
                            </tr>
                            @foreach(\App\PhoneRelation::where('user_id',Auth::user()->id)->orwhere('phone_number',Auth::user()->mobile)->get() as $relation)
                                @if($relation->user_id == Auth::user()->id)
                                    <tr>
                                        <td>{{$relation->relationship_type->name}}</td>
                                        <td>{{$relation->phone_number}}</td>
                                        <td>
                                            @if(\App\User::where('mobile',$relation->phone_number)->first())
                                                {{$relation->user->first_name}} {{$relation->user->last_name}}
                                            @else
                                                Your {{$relation->relationship_type->name}} has not joined yet
                                            @endif
                                        </td>
                                        <td>
                                            @if($relation->approval == 0)
                                                <span class="label label-danger">Not Approved</span>
                                            @elseif($relation->approval == 1)
                                                <span class="label label-success">Approved</span>
                                            @elseif($relation->approval == 2)
                                                <span class="label label-danger">Rejected</span>
                                            @endif
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>
                                            @if($relation->owner->sex == 1)
                                                @if($relation->relationship_type->id == 5)
                                                    Husband
                                                @endif
                                                @if($relation->relationship_type->id == 4)
                                                    Brother
                                                @endif
                                                @if($relation->relationship_type->id == 3)
                                                    Brother
                                                @endif
                                                @if($relation->relationship_type->id == 7)
                                                    Father
                                                @endif
                                                @if($relation->relationship_type->id == 8)
                                                    Father
                                                @endif
                                                @if($relation->relationship_type->id == 2)
                                                    Mother
                                                @endif
                                                @if($relation->relationship_type->id == 1)
                                                    Son
                                                @endif
                                            @else
                                                @if($relation->relationship_type->id == 7)
                                                    Mother
                                                @endif
                                                @if($relation->relationship_type->id == 8)
                                                    Mother
                                                @endif
                                                @if($relation->relationship_type->id == 6)
                                                    Wife
                                                @endif
                                                @if($relation->relationship_type->id == 1)
                                                    Daughter
                                                @endif
                                                @if($relation->relationship_type->id == 2)
                                                    Daughter
                                                 @endif
                                                @if($relation->relationship_type->id == 3)
                                                    Sister
                                                 @endif
                                            @endif

                                        <td>{{$relation->owner->mobile}}</td>
                                        <td>
                                            {{$relation->owner->first_name}} {{$relation->owner->last_name}}
                                        </td>
                                        <td>
                                            @if($relation->approval == 0)
                                                <span class="label label-danger">Not Approved</span>
                                            @elseif($relation->approval == 1)
                                                <span class="label label-success">Approved</span>
                                            @elseif($relation->approval == 2)
                                                <span class="label label-danger">Rejected</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($relation->approval == 0)
                                                <a href="{{route('approve',encrypt($relation->id))}}"><button class="btn btn-success">Approve</button></a>
                                                <a href="{{route('ignore',encrypt($relation->id))}}"><button class="btn btn-warning">Ignore</button></a>
                                            @elseif($relation->approval == 1)
                                                <a href="{{route('ignore',encrypt($relation->id))}}"><button class="btn btn-warning">Ignore</button></a>
                                            @elseif($relation->approval == 2)
                                                <a href="{{route('approve',encrypt($relation->id))}}"><button class="btn btn-success">Approve</button></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop