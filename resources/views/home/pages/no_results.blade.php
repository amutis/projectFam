@extends('home.main')


@section('content')
    <div class="content-wrapper">
        <div class="container">
            <!-- Content Header (Page header) -->

            <!-- Main content -->
            <section class="content">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>

                    <p>The number searched cannot be found.</p>
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.container -->
    </div>
@stop