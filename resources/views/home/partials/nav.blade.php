<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                @if(Auth::check())
                <a href="{{route('relations')}}" class="navbar-brand"><b>Project</b>Fam</a>
                @else
                    <a href="/" class="navbar-brand"><b>Project</b>Fam</a>
                @endif
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">

                    <li class=""><a href="#">News Feed </a></li>
                    @if(Auth::check())
                    <li class=""><a href="{{route('over_view')}}">My Tree </a></li>
                    <li><a href="{{route('relations')}}">My relations</a></li>
                    @endif
                </ul>
                @if(Auth::check())
                {!! Form::open(['route' => 'search','class'=>'navbar-form navbar-left','role'=>'search']) !!}
                <div class="form-group">
                            <input type="text" class="form-control" id="navbar-search-input" name="search" placeholder="Search 254...">
                            <button class="btn btn-info" type="submit">Search</button>
                    </div>
                {!! Form::close() !!}
                @endif
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- Notifications Menu -->
                    @if(Auth::check())

                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">{{\App\PhoneRelation::where('phone_number',Auth::user()->mobile)->where('approval',0)->count()}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have {{\App\PhoneRelation::where('phone_number',Auth::user()->mobile)->where('approval',0)->count()}} relationship notifications</li>
                                <li>
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="menu">
                                        @foreach(\App\PhoneRelation::where('user_id',Auth::user()->id)->orwhere('phone_number',Auth::user()->mobile)->where('approval',0)->get() as $relation)
                                            @if($relation->user_id == Auth::user()->id)
                                            @else

                                            <li><!-- start notification -->
                                                <a href="{{route('approve',encrypt($relation->id))}}">
                                                    {{--<i class="fa fa-users text-aqua"></i>--}}
                                                    Approve
                                                    @if($relation->owner->sex == 1)
                                                        @if($relation->relationship_type->id == 5)
                                                            <span class="label label-info">Husband</span>
                                                        @endif
                                                        @if($relation->relationship_type->id == 4)
                                                            <span class="label label-info">Brother</span>
                                                        @endif
                                                        @if($relation->relationship_type->id == 7)
                                                            <span class="label label-info">Father</span>
                                                        @endif
                                                    @else
                                                        @if($relation->relationship_type->id == 7)
                                                            <span class="label label-info">Mother</span>
                                                        @endif
                                                        @if($relation->relationship_type->id == 8)
                                                            <span class="label label-info">Mother</span>
                                                        @endif
                                                        @if($relation->relationship_type->id == 6)
                                                            <span class="label label-info">Wife</span>
                                                        @endif
                                                        @if($relation->relationship_type->id == 1)
                                                            <span class="label label-info">Daughter</span>
                                                        @endif
                                                        @endif
                                                    request
                                                </a>
                                            </li>

                                            @endif
                                        @endforeach

                                        <!-- end notification -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="{{route('relations')}}">View all</a></li>
                            </ul>
                        </li>
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                    <p>
                                        {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                                        <small>Member since {{date('M. Y',strtotime(Auth::user()->created_at))}}</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{route('profile')}}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li class="active"><a href="/login">Login </a></li>
                        <li class="active"><a href="/register">Create Account</a></li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>