

@if (Session::has('success'))
        <div class="callout callout-success" style="margin-right: 10%;margin-left: 10%; margin-top: 2%">
            <h4>Success!</h4>

            <p>{{Session::get('success')}}</p>
        </div>
@endif