<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function county()
    {
        return $this->hasMany('App\County','country_id','id');
    }


}
