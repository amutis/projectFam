<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class County extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function country()
    {
        return $this->hasOne('App\User','id','country_id');
    }

    /**
     *
     **/
    public function users()
    {
        return $this->hasMany('App\User','county_id','id');
    }
}
