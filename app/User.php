<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','sex',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     **/
    public function user_type()
    {
        return $this->hasOne('App\UserType','id','user_type_id');
    }

    /**
     *
     **/
    public function brothers()
    {
        return $this->hasOne('App\Brother','user_id','id');
    }

    /**
     *
     **/
    public function sisters()
    {
        return $this->hasOne('App\Sister','user_id','id');
    }

    /**
     *
     **/
    public function county()
    {
        return $this->hasOne('App\County','id','county_id');
    }

    /**
     *
     **/
    public function posts()
    {
        return $this->hasMany('App\Brother','author_id','id');
    }
}
