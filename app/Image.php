<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function post()
    {
        return $this->hasOne('App\Post','id','post_id');
    }
}
