<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Spouse extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function spouse_detail()
    {
        return $this->hasOne('App\User','id','spouse_id');
    }
}
