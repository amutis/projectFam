<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Child extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function child_detail()
    {
        return $this->hasOne('App\User','id','child_id');
    }

    /**
     *
     **/
    public function parent_detail()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
