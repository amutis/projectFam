<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *
     **/
    public function search(Request $request)
    {
        $this->validate($request, array(
            'search' => 'required',
        ));

        $user = User::where('mobile',$request->search)->first();

        if ($user != null){
            return view('home.pages.results')
                ->withUser($user);
        }else{
            return view('home.pages.no_results')
                ->withUser($user);
        }

    }
}
