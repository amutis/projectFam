<?php

namespace App\Http\Controllers\Home;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller
{
    /**
     *
     **/
    public function index()
    {
        return view('home.pages.index');
    }


}
