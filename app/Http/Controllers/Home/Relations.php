<?php

namespace App\Http\Controllers\Home;

use App\Child;
use App\Guardian;
use App\PhoneRelation;
use App\Sibling;
use App\Spouse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Relations extends Controller
{
    /**
     *
     **/
    public function getRelationshipPage()
    {
        return view('home.pages.relations');
    }

    /**
     *
     **/
    public function postRelation(Request $request)
    {
        $this->validate($request, array(
            'relation_type' => 'required',
//            'phone_number' => 'required|max:12|min:12',
        ));

        $relation = new PhoneRelation();
        $relation->user_id = Auth::user()->id;
        $relation->relationship_type_id = decrypt($request->relation_type);
        $relation->phone_number =$request->phone_number;
        $relation->save();

        return redirect()->route('relations');
    }

    /**
     *
     **/
    public function approve($id)
    {

        $id = decrypt($id);

        $relation = PhoneRelation::find($id);
        $relation->approval = 1;
        $relation->save();

        if ($relation->relationship_type_id == 1){
            //Owner says child
            $child = new Child();
            $child->user_id = Auth::user()->id;
            $child->child_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $child->save();

            // child needs parent
            $parent = new Guardian();
            $parent->user_id = $child->child_id;
            $parent->guardian_id = $child->user_id;
            $parent->save();



        }elseif ($relation->relationship_type_id == 2){
            //Owner says child
            $child = new Child();
            $child->user_id = Auth::user()->id;
             $child->child_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $child->save();

            // child needs parent
            $parent = new Guardian();
            $parent->user_id = $child->child_id;
            $parent->guardian_id = $child->user_id;
            $parent->save();

        }elseif ($relation->relationship_type_id == 3){
            //Owner says sibling
            $sibling = new Sibling();
            $sibling->user_id = Auth::user()->id;
            $sibling->sibling_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $sibling->save();

            //Sibling needs other sibling
            $sibling2 = new Sibling();
            $sibling2->user_id = $sibling->sibling_id;
            $sibling2->sibling_id = $sibling->user_id;
            $sibling2->save();
        }elseif ($relation->relationship_type_id == 4){
            //Owner says sibling
            $sibling = new Sibling();
            $sibling->user_id = Auth::user()->id;
            $sibling->sibling_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $sibling->save();

            //Sibling needs other sibling
            $sibling2 = new Sibling();
            $sibling2->user_id = $sibling->sibling_id;
            $sibling2->sibling_id = $sibling->user_id;
            $sibling2->save();
        }elseif ($relation->relationship_type_id == 5){
            //Owner says spouse
            $spouse = new Spouse();
            $spouse->user_id = Auth::user()->id;
            $spouse->spouse_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $spouse->save();

            //other spouse
            $spouse2 = new Spouse();
            $spouse2->user_id =$spouse->spouse_id;
            $spouse2->spouse_id = $spouse->user_id;
            $spouse2->save();
        }elseif ($relation->relationship_type_id == 6){
            //Owner says spouse
            $spouse = new Spouse();
            $spouse->user_id = Auth::user()->id;
            $spouse->spouse_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $spouse->save();

            //other spouse
            $spouse2 = new Spouse();
            $spouse2->user_id =$spouse->spouse_id;
            $spouse2->spouse_id = $spouse->user_id;
            $spouse2->save();

        }elseif ($relation->relationship_type_id == 7){
            //Owner says guardian
            $parent = new Guardian();
            $parent->user_id = Auth::user()->id;
            $parent->guardian_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $parent->save();

            //Guardian need the child
            $child = new Child();
            $child->user_id = $parent->guardian_id;
            $child->child_id = Auth::user()->id;
            $child->save();

        }elseif ($relation->relationship_type_id == 8){
            //Owner says guardian
            $parent = new Guardian();
            $parent->user_id = Auth::user()->id;
            $parent->guardian_id = User::where('mobile',$relation->owner->mobile)->first()->id;
            $parent->save();

            //Guardian need the child
            $child = new Child();
            $child->user_id = $parent->guardian_id;
            $child->child_id = Auth::user()->id;
            $child->save();

        }





        Session::flash('success', $relation->owner->first_name. ' has been added as a relative');
        return redirect()->route('relations');

    }

    /**
     *
     **/
    public function ignore($id)
    {
        $id = decrypt($id);

        $relation = PhoneRelation::find($id);
        $relation->approval = 2;
        $relation->save();

        if ($relation->relationship_type_id == 1){
            //Owner says child
            $child = Child::where('child_id',$relation->owner->id)->where('user_id',Auth::user()->id)->first();

            // child needs parent
            $parent = Guardian::where('guardian_id',$child->user_id)->where('user_id',$child->child_id)->first();

            Child::destroy($child->id);
            Guardian::destroy($parent->id);



        }elseif ($relation->relationship_type_id == 2){
            //Owner says child
            $child = Child::where('child_id',$relation->owner->id)->where('user_id',Auth::user()->id)->first();

            // child needs parent
            $parent = Guardian::where('guardian_id',$child->user_id)->where('user_id',$child->child_id)->first();

            Child::destroy($child->id);
            Guardian::destroy($parent->id);


        }elseif ($relation->relationship_type_id == 3){
            //Owner says sibling
            $sibling = Sibling::where('sibling_id',$relation->owner->id)->where('user_id',Auth::user()->id)->first();

            //Sibling needs other sibling
            $sibling2 = Sibling::where('user_id',$sibling->sibling_id)->where('sibling_id',$sibling->user_id)->first();

            Sibling::destroy($sibling->id);
            Sibling::destroy($sibling2->id);

        }elseif ($relation->relationship_type_id == 4){
            //Owner says sibling
            $sibling = Sibling::where('sibling_id',$relation->owner->id)->where('user_id',Auth::user()->id)->first();

            //Sibling needs other sibling
            $sibling2 = Sibling::where('user_id',$sibling->sibling_id)->where('sibling_id',$sibling->user_id)->first();

            Sibling::destroy($sibling->id);
            Sibling::destroy($sibling2->id);

        }elseif ($relation->relationship_type_id == 5){
            //Owner says spouse
            $spouse = Spouse::where('spouse_id',$relation->owner->id)->where('user_id',Auth::user()->id)->first();

            //other spouse
            $spouse2 = Spouse::where('user_id',$spouse->spouse_id)->where('spouse_id',$spouse->user_id)->first();
            Spouse::destroy($spouse->id);
            Spouse::destroy($spouse2->id);
        }elseif ($relation->relationship_type_id == 6){
            //Owner says spouse
            $spouse = Spouse::where('spouse_id',$relation->owner->id)->where('user_id',Auth::user()->id)->first();

            //other spouse
            $spouse2 = Spouse::where('user_id',$spouse->spouse_id)->where('spouse_id',$spouse->user_id)->first();
            Spouse::destroy($spouse->id);
            Spouse::destroy($spouse2->id);

        }elseif ($relation->relationship_type_id == 7){
            //Owner says guardian
            $parent = Guardian::where('user_id',Auth::user()->id)->where('guardian_id',$relation->owner->id)->first();

            //Guardian need the child
            $child = Child::where('user_id',$parent->guardian_id)->where('child_id',Auth::user()->id)->first();
            Guardian::destroy($parent->id);
            Child::destroy($child->id);

        }elseif ($relation->relationship_type_id == 8){
            //Owner says guardian
            $parent = Guardian::where('user_id',Auth::user()->id)->where('guardian_id',$relation->owner->id)->first();

            //Guardian need the child
            $child = Child::where('user_id',$parent->guardian_id)->where('child_id',Auth::user()->id)->first();
            Guardian::destroy($parent->id);
            Child::destroy($child->id);

        }




        Session::flash('success', $relation->owner->first_name. 'has been ignored');
        return redirect()->route('relations');
    }

    /**
     *
     **/
    public function getOverView()
    {
        return view('home.pages.overview');
    }
}
