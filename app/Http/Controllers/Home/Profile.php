<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Profile extends Controller
{
    /**
     *
     **/
    public function getProfile()
    {
        return view('home.pages.profile');
    }

    /**
     *
     **/
    public function update(Request $request)
    {

        $this->validate($request, array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|max:12|min:12',
        ));

        $user = Auth::user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->mobile = $request->phone_number;
        $user->save();

        return redirect()->route('profile');
    }
}
