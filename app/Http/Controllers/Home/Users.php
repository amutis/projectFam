<?php

namespace App\Http\Controllers\Home;

use App\PhoneRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Users extends Controller
{
    /**
     *
     **/
    public function updateInformation(Request $request)
    {
        $this->validate($request, array(
            'date_of_birth' => 'required',
            'family_name' => 'required',
            'phone_number' => 'required|max:12|min:12',
            'county' => 'required',
        ));

        $user = Auth::user();
        $user->mobile = $request->phone_number;
        $user->family_name = $request->family_name;
        $user->county_id = decrypt($request->county);
        $user->dob = $request->date_of_birth;
        $user->save();

        return redirect()->route('relations');
    }
}
