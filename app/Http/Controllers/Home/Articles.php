<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Articles extends Controller
{
    /**
     *
     **/
    public function getPostArticle()
    {
        return view('home.pages.create_article');
    }

    /**
     *
     **/
    public function postArticle(Request $request)
    {
        return $request->article;
    }
}
