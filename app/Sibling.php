<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sibling extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function sibling_detail()
    {
        return $this->hasOne('App\User','id','sibling_id');
    }
}
