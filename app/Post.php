<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     *
     **/
    public function images()
    {
        return $this->hasOne('App\Image','post_id','id');
    }

    /**
     *
     **/
    public function user()
    {
        return $this->hasOne('App\User','id','author_id');
    }

    /**
     *
     **/
    public function category()
    {
        return $this->hasOne('App\Category','id','category_id');
    }
}
