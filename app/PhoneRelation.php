<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneRelation extends Model
{
    /**
     *
     **/
    public function relationship_type()
    {
        return $this->hasOne('App\RelationshipType','id','relationship_type_id');
    }

    /**
     *
     **/
    public function user()
    {
        return $this->hasOne('App\User','mobile','phone_number');
    }

    /**
     *
     **/
    public function owner()
    {
        return $this->hasOne('App\User','id','user_id');
    }


}
