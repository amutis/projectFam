<?php

use Illuminate\Database\Seeder;

class CountySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('counties')->truncate();

         $data = [
             ['country_id' => '116', 'name' => 'Nairobi'],
             ['country_id' => '116', 'name' => 'Mombasa'],
             ['country_id' => '116', 'name' => 'Uasin Gishu'],
             ['country_id' => '116', 'name' => 'Bungoma'],
         ];

         DB::table('counties')->insert($data);
    }
}
