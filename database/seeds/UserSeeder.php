<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->truncate();

         $data = [
             ['mobile' => 'ely','family_name' => 'project fam', 'first_name' => 'Elyves','last_name' => 'Situma','email' => 'ely@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '1'],
             ['mobile' => 'sar','family_name' => 'project fam', 'first_name' => 'Sarah','last_name' => 'Wanjiru','email' => 'sar@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '2'],
             ['mobile' => 'dav','family_name' => 'project fam', 'first_name' => 'David','last_name' => 'Situma','email' => 'dav@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '1'],
             ['mobile' => 'jon','family_name' => 'project fam', 'first_name' => 'Jonathan','last_name' => 'Wafula','email' => 'jon@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '1'],
             ['mobile' => 'mer','family_name' => 'project fam', 'first_name' => 'Mercy','last_name' => 'Nyambura','email' => 'mer@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '2'],
             ['mobile' => 'han','family_name' => 'project fam', 'first_name' => 'Hannah','last_name' => 'Karoki','email' => 'han@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '2'],
             ['mobile' => 'geo','family_name' => 'project fam', 'first_name' => 'George','last_name' => 'Njogu','email' => 'geo@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '1'],
             ['mobile' => 'nan','family_name' => 'project fam', 'first_name' => 'Nancy','last_name' => 'Karoki','email' => 'nan@gmail.com','password' => '$2y$10$pzgggqaRbqMKr/As4uzX1.5edFX7p5cIrurLKTxJI0kzu7tG2.1pm','dob' => '1990-01-01','county_id' => '1','sex' => '2'],
         ];

         DB::table('users')->insert($data);
    }
}
