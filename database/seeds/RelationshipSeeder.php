<?php

use Illuminate\Database\Seeder;

class RelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('relationship_types')->truncate();

         $data = [
             ['name' => 'Father', 'description' => 'data'],
             ['name' => 'Mother', 'description' => 'data'],
             ['name' => 'Brother', 'description' => 'data'],
             ['name' => 'Sister', 'description' => 'data'],
             ['name' => 'Wife', 'description' => 'data'],
             ['name' => 'Husband', 'description' => 'data'],
             ['name' => 'Son', 'description' => 'data'],
             ['name' => 'Daughter', 'description' => 'data'],
             ['name' => 'Guardian', 'description' => 'data'],
         ];

         DB::table('relationship_types')->insert($data);
    }
}
