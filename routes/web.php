<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'Home\Index@index');
Route::get('/blog', 'HomeController@index');

Route::get('article/post', 'Home\Articles@getPostArticle')->name('post_article');
Route::post('article/post', 'Home\Articles@postArticle')->name('post_article');

Route::get('relations', 'Home\Relations@getRelationshipPage')->name('relations');
Route::post('article/post', 'Home\Articles@postArticle')->name('post_article');

Route::post('update/information', 'Home\Users@updateInformation')->name('update_information');
Route::post('update/relations', 'Home\Relations@postRelation')->name('add_relationship');

Route::get('approve/relation/{id}', 'Home\Relations@approve')->name('approve');
Route::get('ignore/relation/{id}', 'Home\Relations@ignore')->name('ignore');
Route::get('overview', 'Home\Relations@getOverView')->name('over_view');
Route::get('profile', 'Home\Profile@getProfile')->name('profile');
Route::post('update_profile', 'Home\Profile@update')->name('update_profile');

Route::post('search', 'SearchController@search')->name('search');



Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

